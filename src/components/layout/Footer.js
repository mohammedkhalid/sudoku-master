import React from 'react';

/**
 * React component for the Footer Section.
 */
const Footer = () => {
  return (
    <footer className="footer">
      <p>&#169; 2020 Amith Raravi - source code on <a href="https://github.com/raravi/sudoku">Github</a></p>
    </footer>
  )
}

export default Footer;